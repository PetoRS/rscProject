<?php
//PHP errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();
?>
<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/login/css/login.css">
    <title>Log In</title>
</head>
<body>
<div id="container">
    <div class="login_modal">
        <div id="login_logInDiv" class="login_outerDiv">
            <div class="login_title">Log In</div>
            <hr class="login_hr">
            <?php
            require_once 'assets/login/php/actions.php';
            ?>
            <form action="login.php" method="post">
                <div class="form-group">
                    <input type="hidden" name="action" value="logIn">
                    <div>
                        <div class="login_divIcons">
                            <div class="glyphicon glyphicon-user login_formIcons"></div>
                        </div>
                        <input class="form-control login_textInputs" name="login" placeholder="Email | Username">
                    </div>

                    <div>
                        <div class="login_divIcons">
                            <div class="glyphicon glyphicon-lock login_formIcons"></div>
                        </div>
                        <input type="password" class="form-control login_textInputs" name="password"
                               placeholder="Password">
                    </div>
                    <button id="login_logInButton" class="login_buttons btn btn-default">LOGIN</button>
                </div>
            </form>
            <div class="login_footer">
                <p>Looking to <span id="redirectToRegister" class="login_link">create an account</span> ?</p>
                <p>Continue <span id="redirectToEditor" class="login_link">without </span> login</p>
            </div>
        </div>

        <div style="display: none;" id="login_registerDiv" class="login_outerDiv">
            <div class="login_title">Registration</div>
            <hr class="login_hr">
            <form action="assets/login/php/actions.php" method="post">
                <div class="form-group">
                    <input type="hidden" name="action" value="register">
                    <div class="login_tooltips">
                        <div class="login_divIcons">
                            <div class="glyphicon glyphicon-user login_formIcons"></div>
                        </div>
                        <input id="form_username" class="form-control login_textInputs" name="username"
                               placeholder="* Username" data-toggle="tooltip" data-placement="right">
                    </div>
                    <div>
                        <div class="login_divIcons">
                            <div class="glyphicon glyphicon-envelope login_formIcons"></div>
                        </div>
                        <input type="email" id="form_email" class="form-control login_textInputs" name="email"
                               placeholder="* Email" data-toggle="tooltip" data-placement="right">
                    </div>
                    <div>
                        <div class="login_divIcons">
                            <div class="glyphicon glyphicon-user login_formIcons"></div>
                        </div>
                        <input type="text" class="form-control login_textInputs" name="name" placeholder="Name">
                    </div>
                    <div>
                        <div class="login_divIcons">
                            <div class="glyphicon glyphicon-user login_formIcons"></div>
                        </div>
                        <input type="text" class="form-control login_textInputs" name="surname" placeholder="Surname">
                    </div>
                    <div>
                        <div class="login_divIcons">
                            <div class="glyphicon glyphicon-lock login_formIcons"></div>
                        </div>
                        <input type="password" id="form_password" class="form-control login_textInputs" minlength="4"
                               name="password" placeholder="* Password">
                    </div>
                    <div>
                        <div class="login_divIcons">
                            <div class="glyphicon glyphicon-lock login_formIcons"></div>
                        </div>
                        <input type="password" id="form_retypePassword" class="form-control login_textInputs" name=""
                               placeholder="* Re-type password">
                    </div>
                    <button disabled id="registerButton" class="login_buttons btn btn-default">REGISTER</button>
                </div>
            </form>
            <div class="login_footer">
                <p>Already have an account? <span id="redirectToLogin" class="login_link">Login</span></p>
            </div>
        </div>
    </div>
</div>
<script src="assets/global/dependencies/jquery/jquery-3.2.1.min.js"></script>
<!--<script src="assets/global/dependencies/materialize-src/js/bin/materialize.min.js"></script>-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="dist/loginBundle.js"></script>
</body>
</html>