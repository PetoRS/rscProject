import Column from './Column';
import Database from './Database';
import Row from './Row';
import { replaceOneWordOccurence, getEditorSelectedText, getMatchedWordCount, clearTimerAndShowTooltip } from './editor.js';

/*************************
 LISTENERS
 RSC variable row
 RSC variable row adding
 RSC variable row editing
 Category row adding
 Category row editing
 *************************/

export default class Listeners {
    /*
     * RSC variable row
     */
    /* Mark "clickedElement" as last used and hide "dropdown_container" */
    static dropdown_labels_onclick(clickedElement) {
        // send chosen "rsc variable" to editor
        let editor = CKEDITOR.instances['inputText'];
        //editor.execCommand("replace");
        // document.cookie = "clickedRSC="+clickedElement.innerText;

        // Export a import ES6 je potrebné ešte zisti príčinu nefunkčnosti
        if (getEditorSelectedText(editor)) {
            if (getMatchedWordCount(editor.document.getBody().getText(),editor.getSelection().getSelectedText(),false) === 1){
                replaceOneWordOccurence(editor,clickedElement.innerText);
            } else {
                document.cookie = "clickedRSC=" + clickedElement.innerText;
                editor.execCommand('replace');
            }
        } else {
            // clearTimerAndShowTooltip();
            alert("Nebol označeny žiadny text!");
        }

        /* mark whole row as last used element with class "dropdown_lastUsed" */
        const lastUsedElement = document.getElementsByClassName("dropdown_lastUsed");
        // remove "dropdown_lastUsed" class from last used item
        if (lastUsedElement.length !== 0)
            lastUsedElement[0].classList.remove("dropdown_lastUsed");

        const dropdown_row = clickedElement.parentNode;
        dropdown_row.classList.add("dropdown_lastUsed");

        // finally hide "dropdown_container"
        document.getElementById("dropdown_container").classList.toggle("show");
    }

    /*
     * RSC variable row adding
     */
    /* Onclick hide "text input" row, clear its content and display "Add +" row, add new record to database */
    static iconOk_addRow_onclick(clickedElement) {
        let dropdown_addRow = clickedElement.parentNode.previousSibling;
        // display "Add +" row
        dropdown_addRow.classList.toggle("hide");

        // <input type="text"> element
        let dropdown_inputText = clickedElement.previousSibling.previousSibling.firstChild;
        let newRowName = dropdown_inputText.value;

        // insert new "dropdown_row" before "Add +" row
        let dropdown_column = clickedElement.parentNode.parentNode;
        dropdown_column.insertBefore( Row.createRow(newRowName), dropdown_addRow );

        // add new row to database
        // get "categoryId" from "dropdown_column" ID
        let categoryId = Column.getCategoryIdFromColumnId(dropdown_column.id);
        Database.insert_dropdownRow( newRowName, categoryId );

        // clear text input after use and hide "input row" which contains "text input row"
        let dropdown_inputRow = clickedElement.parentNode;
        dropdown_inputRow.classList.toggle("hide");

        dropdown_inputText.value = "";
    }
    /* Onclick hide "text input" row, clear its content and display "Add +" row */
    static iconRemove_addRow_onclick(clickedElement) {
        let dropdown_addRow = clickedElement.parentNode.previousSibling;
        // display "Add +" row
        dropdown_addRow.classList.toggle("hide");

        // clear text input after use
        let dropdown_inputText = clickedElement.previousSibling.firstChild;
        dropdown_inputText.value = "";

        // hide "text input row"
        let dropdown_inputRow = clickedElement.parentNode;
        dropdown_inputRow.classList.toggle("hide");
    }

    /*
     * RSC variable row editing
     */
    /* Onclick hide "Text input row" and display "rsc variable" row back*/
    static iconOk_editRow_onclick(clickedElement, oldRsc) {
        const dropdownRow = clickedElement.parentNode.previousSibling;

        const inputRow = clickedElement.parentNode;
        const dropdownColumn = inputRow.parentNode;

        // use text input value as new name in "rsc variable" row
        const inputText = clickedElement.previousSibling.previousSibling.firstChild;
        const newRsc = inputText.value;

        // delete "input row"
        dropdownColumn.removeChild(inputRow);

        // "dropdown_labels"
        const dropdown_labels = dropdownRow.firstChild;
        dropdown_labels.innerText = newRsc;

        // update row in database
        Database.update_dropdownRow(oldRsc, newRsc);

        // display "rsc variable" row back
        dropdownRow.classList.toggle("hide");
    }
    /* Onclick hide current row, display instead "text input row" */
    static iconPencil_editRow_onclick(clickedElement) {
        let dropdown_row = clickedElement.parentNode;
        dropdown_row.classList.toggle("hide");

        let rscVariable = dropdown_row.firstChild.innerText;

        let textInputRow = Row.createTextInputRow_editRow(rscVariable);
        textInputRow.classList.toggle("hide");

        dropdown_row.parentNode.insertBefore(textInputRow, dropdown_row.nextSibling);

        let textInput = textInputRow.firstChild.firstChild;
        textInput.focus();
    }
    /* Onclick hide "Text input row" and display "rsc variable" row back*/
    static iconRemove_editRow_onclick(clickedElement) {
        const dropdownRow = clickedElement.parentNode.previousSibling;
        // display "Category name" row
        dropdownRow.classList.toggle("hide");

        // delete "input row"
        const inputRow = clickedElement.parentNode;
        const dropdownColumn = inputRow.parentNode;

        dropdownColumn.removeChild(inputRow);
    }
    /* Delete whole "dropdown_row" from JS and Database and hide "dropdown_container" */
    static iconTrash_deleteRow_onclick(clickedElement) {
        const dropdown_row = clickedElement.parentNode;
        const dropdown_column = dropdown_row.parentNode;

        // get "rsc variable"
        const rsc_variableName = clickedElement.previousSibling.previousSibling.innerText;

        // find out "rsc_categoryId" from "dropdown_column" id, must be same as id in database
        let rsc_categoryId = Column.getCategoryIdFromColumnId(dropdown_column.id);

        // delete row record from database
        Database.delete_dropdownRow(rsc_variableName, rsc_categoryId);

        // finally delete row from javascript
        dropdown_column.removeChild(dropdown_row);
    }

    /*
     * Category Row adding
     */
    /* Onclick add new "dropdown column" with "add+ row" */
    static addCategoryRow_onclick(clickedElement) {
        //hide "Add category row" and display "Text input row"
        clickedElement.classList.toggle("hide");
        clickedElement.nextSibling.classList.toggle("hide");
    }
    /* Onclick create new "dropdown column" and hide "text input row" */
    static iconOk_addCategory_onclick(clickedElement) {
        let categoryName = clickedElement.previousSibling.previousSibling.firstChild.value;

        Database.insert_dropdownCategoryName(categoryName);

        // display category row back
        const dropdown_categoryRow = clickedElement.parentNode.previousSibling;

        // clear text input after use and hide input row
        const dropdown_inputRow = clickedElement.parentNode;
        dropdown_inputRow.classList.toggle("hide");

        const inputText = clickedElement.previousSibling.previousSibling.firstChild;
        inputText.value = "";

        // display "category row"
        dropdown_categoryRow.classList.toggle("hide");
    }
    /* Onclick hide and clear "text input row" and display back "category add+ row" */
    static iconRemove_addCategory_onclick(clickedElement) {
        let dropdown_categoryRow = clickedElement.parentNode.previousSibling;
        // display "Category name" row
        dropdown_categoryRow.classList.toggle("hide");

        // clear text input after use
        let inputText = clickedElement.previousSibling.firstChild;
        inputText.value = "";

        // hide input row
        let inputRow = clickedElement.parentNode;
        inputRow.classList.toggle("hide");
    }

    /*
     * Category Row editing
     */
    /* Onclick hide "text input" row, clear its content and display "Category name" row, update in database*/
    static iconOk_editCategory_onclick(clickedElement) {
        // display category row back
        const dropdown_categoryRow = clickedElement.parentNode.previousSibling;

        // use text input value as new name in "Category name" row
        const inputText = clickedElement.previousSibling.previousSibling.firstChild;
        dropdown_categoryRow.firstChild.innerText = inputText.value;

        // add new row to database
        // get "categoryId" from "dropdown_column" ID
        let categoryId = Column.getCategoryIdFromColumnId(dropdown_categoryRow.parentNode.id);
        Database.update_dropdownCategoryName(categoryId, inputText.value);

        // clear text input after use and hide input row
        const dropdown_inputRow = clickedElement.parentNode;
        dropdown_inputRow.classList.toggle("hide");
        inputText.value = "";

        // display "category row"
        dropdown_categoryRow.classList.toggle("hide");
    }
    /* Onclick display "text input" row with actual "category name", hide "category name" row */
    static iconPencil_editCategory_onclick(clickedElement) {
        // hide row with category name
        clickedElement.parentNode.classList.toggle("hide");

        // display "text input" row for editing with actual "category name"
        const inputRow = clickedElement.parentNode.nextSibling;
        inputRow.classList.toggle("hide");

        const textInput = inputRow.firstChild.firstChild;
        const categoryName = clickedElement.previousSibling.innerText;

        // fill text input with existing "Category name" and place cursor to the end
        textInput.value = categoryName;
        textInput.focus();
    }
    /* Onclick hide "text input" row, clear its content and display "Category name" row */
    static iconRemove_editCategory_onclick(clickedElement) {
        let dropdown_categoryRow = clickedElement.parentNode.previousSibling;
        // display "Category name" row
        dropdown_categoryRow.classList.toggle("hide");

        // clear text input after use
        let inputText = clickedElement.previousSibling.firstChild;
        inputText.value = "";

        // hide input row
        let inputRow = clickedElement.parentNode;
        inputRow.classList.toggle("hide");
    }
    /* Onclick delete "dropdown column" from JS and database */
    static iconTrash_deleteCategory_onclick(clickedElement) {
        const dropdown_container = document.getElementById("dropdown_container");
        const columnToDelete = clickedElement.parentNode.parentNode;

        Database.delete_dropdownCategory( Column.getCategoryIdFromColumnId(columnToDelete.id) );

        dropdown_container.removeChild(columnToDelete);

        Column.resizeDropdown();
    }
}