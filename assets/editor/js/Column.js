import Listeners from './Listeners';
import Row from './Row';

export default class Column {
    /* If there are less than 5 categories accommodate "dropdown container" length */
    static getDropdownContainerWidth(categoriesNum) {
        if (categoriesNum <= 4)
            return 138 * categoriesNum;
        else
            return 691;
    }

    static getColumnWidth(categoriesNum) {
        if (categoriesNum <= 4)
            return 100/categoriesNum * 0.955;
        else
            return 19;
    }

    /* Find out "rsc_categoryId" from "dropdown_column" id, must be same as id in database */
    static getCategoryIdFromColumnId(columnId) {
        let rsc_categoryId = columnId.split("_");
        return rsc_categoryId[2];
    }
    /* Create column
     * PARAMETERS: column_width - width of one column
     *             id - "dropdown_column_" + "id"
     */
    static createColumn(id) {
        //    <div id="dropdown_column_ID" class="dropdown_columns">
        //    </div>
        const dropdown_column = document.createElement("DIV");

        dropdown_column.setAttribute("id", "dropdown_column_" + id);
        dropdown_column.classList.add("dropdown_columns");

        return dropdown_column;
    }
    /* Create category label, if the name is reserved "Own" then append it to the right
     * PARAMETERS: category_name
     */
    static createCategoryLabel(categoryName) {
        // <div class="dropdown_categoryRow">
        //    <div class="dropdown_categoryLabel"
        //    CategoryName
        //    </div>
        //    <div class="glyphicon glyphicon-pencil dropdown_pencilIcons"></div>
        //    <div class="glyphicon glyphicon-trash dropdown_trashIcons"></div>
        // </div>
        const dropdown_categoryRow = document.createElement("DIV");
        dropdown_categoryRow.classList.add("dropdown_categoryRow");

        const dropdown_categoryLabel = document.createElement("DIV");
        dropdown_categoryLabel.innerText = categoryName;
        dropdown_categoryLabel.classList.add("dropdown_categoryLabel");

        dropdown_categoryRow.appendChild(dropdown_categoryLabel);

        const icon_pencil = document.createElement("DIV");
        icon_pencil.classList.add("glyphicon", "glyphicon-pencil", "dropdown_pencilIcons_category");
        icon_pencil.addEventListener("click", function () {
            Listeners.iconPencil_editCategory_onclick(this);
        });

        const icon_trash = document.createElement("DIV");
        icon_trash.classList.add("glyphicon", "glyphicon-trash", "dropdown_trashIcons_category");
        icon_trash.addEventListener("click", function () {
            Listeners.iconTrash_deleteCategory_onclick(this);
        });

        dropdown_categoryRow.appendChild(icon_pencil);
        dropdown_categoryRow.appendChild(icon_trash);


        return dropdown_categoryRow;
    }
    /* Generate columns with category labels from database
     * PARAMETERS: json - JSON from SQL select */
    static createColumns(json) {
        const dropdown_container = document.getElementById( "dropdown_container" );

        // append other categories labels before "Own" category label
        // iterate through every row in JSON except "Own" category with ID = 1
        const jsonLength = json.length;

        for (let i = 0; i < jsonLength; i++) {
                let dropdown_column = Column.createColumn(json[i]["id"]);

                dropdown_column.appendChild( Column.createCategoryLabel(json[i]["name"]) );
                // append text input for editing column name
                dropdown_column.appendChild( Row.createTextInputRow_edit() );

                dropdown_container.appendChild(dropdown_column);
        }
    }
    /* Generate "Add Column" column */
    static createAddColumn() {
        //    <div id="dropdown_addCategoryColumn" class="dropdown_columns">
        //      <div class="dropdown_categoryRow">
        //          <div class="dropdown_addColumnLabel">
        //              Add Category
        //          </div>
        //          <div class="glyphicon glyphicon-plus dropdown_plusIcons"></div>
        //      </div>
        //    </div>
        const dropdownColumn = document.createElement("DIV");
        dropdownColumn.setAttribute("id", "dropdown_addCategoryRow");
        dropdownColumn.classList.add("dropdown_columns");

        const dropdownCategoryRow = document.createElement("DIV");
        dropdownCategoryRow.classList.add("dropdown_addCategoryRow");
        dropdownCategoryRow.addEventListener("click", function () {
            Listeners.addCategoryRow_onclick(this);
        });

        const dropdownAddColumnLabel = document.createElement("DIV");
        dropdownAddColumnLabel.classList.add("dropdown_addCategoryLabel");
        dropdownAddColumnLabel.innerText = "Add Category";

        const plusIcon = document.createElement("DIV");
        plusIcon.classList.add("glyphicon", "glyphicon-plus", "dropdown_plusIcons");

        dropdownCategoryRow.appendChild(dropdownAddColumnLabel);
        dropdownCategoryRow.appendChild(plusIcon);

        dropdownColumn.appendChild(dropdownCategoryRow);

        return dropdownColumn;
    }

    static resizeDropdown() {
        const dropdown_columns = document.getElementsByClassName("dropdown_columns");
        const categoriesNum = dropdown_columns.length;

        const dropdownContainerWidth = Column.getDropdownContainerWidth(categoriesNum) + "%";
        const dropdownColumnWidth = Column.getColumnWidth(categoriesNum) + "%";

        document.getElementById("dropdown_container").style.minWidth = dropdownContainerWidth;
        document.getElementById("dropdown_container").style.maxWidth = dropdownContainerWidth;

        for (let i=0; i < categoriesNum; i++) {
            dropdown_columns[i].style.minWidth = dropdownColumnWidth;
        }
    }
}