import Column from './Column';
import Row from './Row';

export default class Database {
    /* PARAMETERS: rowName - "dropdown label" string value
     *             categoryId - id of "dropdown_column", must be identical as row in database
     */
    static delete_dropdownRow(rowName, categoryId) {
        $.ajax({
            type: 'POST',
            data: {action: "delete_dropdownRow",
                rowName: rowName,
                categoryId: categoryId},
            url: 'assets/editor/php/database_functions.php',
            success: function (msg) {
                if (msg === false) {
                    alert("Database connection error!");
                }
            }
        });
    }

    /* PARAMETERS: rowName - "dropdown label" string value
     *             categoryId - id of "dropdown_column", must be identical as row in database
     */
    static insert_dropdownRow(rowName, categoryId) {
        $.ajax({
            type: 'POST',
            data: {action: "insert_dropdownRow",
                rowName: rowName,
                categoryId: categoryId},
            url: 'assets/editor/php/database_functions.php',
            success: function (msg) {
                if (msg === false)
                    alert("Database connection error!");
            }
        });
    }

    /* PARAMETERS: rowName - "dropdown label" string value
     *             categoryId - id of "dropdown_column", must be identical as row in database
     */
    static update_dropdownRow(oldRowName, newRowName) {
        $.ajax({
            type: 'POST',
            data: {action: "update_dropdownRow",
                oldRowName: oldRowName,
                newRowName: newRowName
            },
            url: 'assets/editor/php/database_functions.php',
            success: function (msg) {
                if (msg === false)
                    alert("Database connection error!");
            }
        });
    }

    /* PARAMETERS: categoryId - id of "dropdown_column", must be identical as row in database
     *             categoryName - updated name of "dropdown_column"
     */
    static update_dropdownCategoryName(categoryId, categoryName) {
        $.ajax({
            type: 'POST',
            data: {action: "update_dropdownCategoryName",
                categoryName: categoryName,
                categoryId: categoryId},
            url: 'assets/editor/php/database_functions.phpp',
            success: function (msg) {
                if (msg === false)
                    alert("Database connection error!");
            }
        });
    }

    /* PARAMETERS: categoryId - id of "dropdown_column", must be identical as row in database
     */
    static delete_dropdownCategory(categoryId) {
        $.ajax({
            type: 'POST',
            data: {action: "delete_dropdownCategory",
                   categoryId: categoryId},
            url: 'assets/editor/php/database_functions.php',
            success: function (msg) {
                if (msg === false)
                    alert("Database error!");
            }
        });
    }

    /* PARAMETERS: categoryName
     *
     */
    static insert_dropdownCategoryName(categoryName) {
        $.ajax({
            type: 'POST',
            data: {action: "insert_dropdownCategoryName",
                   categoryName: categoryName},
            url: 'assets/editor/php/database_functions.php',
            success: function (msg) {
                let dropdown_column = Column.createColumn(msg);

                dropdown_column.appendChild( Column.createCategoryLabel(categoryName) );
                dropdown_column.appendChild( Row.createTextInputRow_edit() );
                dropdown_column.appendChild( Row.createAddRow() );
                dropdown_column.appendChild( Row.createTextInputRow() );

                const dropdown_container = document.getElementById("dropdown_container");
                dropdown_container.insertBefore(dropdown_column, document.getElementById("dropdown_addCategoryRow"));

                Column.resizeDropdown();
            }
        });
    }

    /* Load all "rsc variables" from database
     * return JSON.parse structure
     */
    static select_dropdownRows() {
        let json;

        $.ajax({
            type: 'POST',
            data: {action: "select_dropdownRows"},
            url: 'assets/editor/php/database_functions.php',
            success: function (data) {
                json = JSON.parse(data);
                Row.createRows(json);
            }
        });

        return json;
    }

    /* Load all "dropdown_columns name" from database
     * return JSON.parse structure
     */
    static select_dropdownCategories() {
        $.ajax({
            type: 'POST',
            data: {action: "select_dropdownCategories"},
            url: 'assets/editor/php/database_functions.php',
            success: function (msg) {
                let json = JSON.parse(msg);

                Column.createColumns(json);
                Database.select_dropdownRows();
            }
        });
    }
}