import Column from './Column';
import Listeners from './Listeners'

export default class Row {
    /* Create new Row with "rsc variable" and trash icon*/
    static createRow(rscVariable) {
        //  <div class="dropdown_rows">
        //      <div class="dropdown_labels">
        //          rscVariable
        //      </div>
        //      <div class="glyphicon glyphicon-pencil dropdown_pencilIcons"></div>
        //      <div class="glyphicon glyphicon-trash dropdown_trashIcons"></div>
        //  </div>
        const dropdown_row = document.createElement("DIV");
        dropdown_row.classList.add("dropdown_rows");

        const dropdown_label = document.createElement("DIV");
        dropdown_label.classList.add("dropdown_labels");
        dropdown_label.innerText = rscVariable;
        dropdown_label.addEventListener("click", function () {
            Listeners.dropdown_labels_onclick(this);
        });

        const icon_pencil = document.createElement("DIV");
        icon_pencil.classList.add("glyphicon", "glyphicon-pencil", "dropdown_pencilIcons");
        icon_pencil.addEventListener("click", function () {
            Listeners.iconPencil_editRow_onclick(this);
        });

        const icon_trash = document.createElement("DIV");
        icon_trash.classList.add("glyphicon", "glyphicon-trash", "dropdown_trashIcons");
        icon_trash.addEventListener("click", function () {
            Listeners.iconTrash_deleteRow_onclick(this);
        });

        dropdown_row.appendChild(dropdown_label);
        dropdown_row.appendChild(icon_pencil);
        dropdown_row.appendChild(icon_trash);

        return dropdown_row;
    }
    /* Generate "dropdown_rows" in dropdown menu from database
     * PARAMETERS: json_rows - JSON structure from SQL select */
    static createRows(json) {
        // iterate through every row in JSON */
        const jsonLength = json.length;
        for (let i = 0; i < jsonLength; i++) {
            // create "dropdown_column" ID where to append "dropdown_row"
            let dropdownColumnId = "dropdown_column_" + json[i]["category_id"];
            let newRow = Row.createRow( json[i]["name"] );

            //append "dropdown_row" to "dropdown_column"
            document.getElementById(dropdownColumnId).appendChild(newRow);
        }

        // add row "Add +" and row "input text" after rows "rsc variables" */
        const dropdown_container = document.getElementById("dropdown_container");

        // iterate through every "#dropdown_container" child */
        const containerChildrenNum = dropdown_container.children.length;
        for (let i = 0; i < containerChildrenNum; i++) {
            dropdown_container.children[i].appendChild( Row.createAddRow() );
            dropdown_container.children[i].appendChild( Row.createTextInputRow() );
        }

        dropdown_container.appendChild(Column.createAddColumn());
        document.getElementById("dropdown_addCategoryRow").appendChild(Row.createTextInputRow_addCategory());

        Column.resizeDropdown();
    }
    /* Create "Add +" row */
    static createAddRow() {
        //    <div class="dropdown_rows">
        //        <div class="dropdown_addLabels">Add</div>
        //        <div class="glyphicon glyphicon-plus dropdown_plusIcons"></div>
        //    </div>
        let dropdown_row = document.createElement("DIV");
        dropdown_row.classList.add("dropdown_addRows");

        let dropdown_addLabel = document.createElement("DIV");
        dropdown_addLabel.classList.add("dropdown_addLabels");
        dropdown_addLabel.innerText = "Add";

        let icon_plus = document.createElement("DIV");
        icon_plus.classList.add("glyphicon", "glyphicon-plus", "dropdown_plusIcons");

        dropdown_row.appendChild(dropdown_addLabel);
        dropdown_row.appendChild(icon_plus);

        // Onclick hide add row and display text input row */
        dropdown_row.addEventListener("click", function () {
            //hide "Add +" row
            this.classList.toggle("hide");
            //display "text input" row
            this.nextSibling.classList.toggle("hide");
        });

        return dropdown_row;
    }
    /* Create "text input" row which shows when add label is clicked on */
    static createTextInputRow() {
        //    <div class="dropdown_inputRow">
        //        <div class="dropdown_inputText">
        //            <input type="text" size="12">
        //        </div>
        //        <div class="glyphicon glyphicon-ok dropdown_okIcons"></div>
        //        <div class="glyphicon glyphicon-remove dropdown_removeIcons"></div>
        //    </div>
        let dropdown_row = document.createElement("DIV");
        dropdown_row.classList.add("dropdown_inputRow");
        // by default its hidden
        dropdown_row.classList.toggle("hide");

        let dropdown_inputText = document.createElement("DIV");
        dropdown_inputText.classList.add("dropdown_inputText");

        let dropdown_input = document.createElement("INPUT");
        dropdown_input.setAttribute("type", "text");
        dropdown_input.setAttribute("size", "18");

        let icon_remove = document.createElement("DIV");
        icon_remove.classList.add("glyphicon", "glyphicon-remove", "dropdown_removeIcons");

        icon_remove.addEventListener("click", function() {
            Listeners.iconRemove_addRow_onclick(this);
        });

        let icon_ok = document.createElement("DIV");
        icon_ok.classList.add("glyphicon", "glyphicon-ok", "dropdown_okIcons");

        icon_ok.addEventListener("click", function() {
            Listeners.iconOk_addRow_onclick(this);
        });

        dropdown_inputText.appendChild(dropdown_input);
        dropdown_row.appendChild(dropdown_inputText);
        dropdown_row.appendChild(icon_remove);
        dropdown_row.appendChild(icon_ok);

        return dropdown_row;
    }
    /* Create "text input" row which shows when "category name" is about to change */
    static createTextInputRow_edit() {
        //    <div class="dropdown_inputRow">
        //        <div class="dropdown_inputText">
        //            <input type="text" size="18">
        //        </div>
        //        <div class="glyphicon glyphicon-ok dropdown_okIcons"></div>
        //        <div class="glyphicon glyphicon-remove dropdown_removeIcons"></div>
        //    </div>
        let dropdown_row = document.createElement("DIV");
        dropdown_row.classList.add("dropdown_categoryRow");
        // by default its hidden
        dropdown_row.classList.toggle("hide");

        let dropdown_inputText = document.createElement("DIV");
        dropdown_inputText.classList.add("dropdown_inputText_edit");

        let dropdown_input = document.createElement("INPUT");
        dropdown_input.setAttribute("type", "text");
        dropdown_input.setAttribute("size", "18");

        let icon_remove = document.createElement("DIV");
        icon_remove.classList.add("glyphicon", "glyphicon-remove", "dropdown_removeIcons");
        icon_remove.addEventListener("click", function(event) {
            Listeners.iconRemove_editCategory_onclick(this);
        });

        let icon_ok = document.createElement("DIV");
        icon_ok.classList.add("glyphicon", "glyphicon-ok", "dropdown_okIcons");
        icon_ok.addEventListener("click", function(event) {
            Listeners.iconOk_editCategory_onclick(this);
        });

        dropdown_inputText.appendChild(dropdown_input);
        dropdown_row.appendChild(dropdown_inputText);
        dropdown_row.appendChild(icon_remove);
        dropdown_row.appendChild(icon_ok);

        return dropdown_row;
    }
    /* Create "text input" row which shows when "category name" is about to be add */
    static createTextInputRow_addCategory() {
        //    <div class="dropdown_inputRow">
        //        <div class="dropdown_inputText_addCategory">
        //            <input type="text" size="16">
        //        </div>
        //        <div class="glyphicon glyphicon-ok dropdown_okIcons"></div>
        //        <div class="glyphicon glyphicon-remove dropdown_removeIcons"></div>
        //    </div>
        let dropdown_row = document.createElement("DIV");
        dropdown_row.classList.add("dropdown_categoryRow");
        // by default its hidden
        dropdown_row.classList.toggle("hide");

        let dropdown_inputText = document.createElement("DIV");
        dropdown_inputText.classList.add("dropdown_inputText_addCategory");

        let dropdown_input = document.createElement("INPUT");
        dropdown_input.setAttribute("type", "text");
        dropdown_input.setAttribute("size", "16");

        let icon_remove = document.createElement("DIV");
        icon_remove.classList.add("glyphicon", "glyphicon-remove", "dropdown_removeIcons");
        icon_remove.addEventListener("click", function(event) {
            Listeners.iconRemove_addCategory_onclick(this);
        });

        let icon_ok = document.createElement("DIV");
        icon_ok.classList.add("glyphicon", "glyphicon-ok", "dropdown_okIcons");
        icon_ok.addEventListener("click", function(event) {
            Listeners.iconOk_addCategory_onclick(this);
        });

        dropdown_inputText.appendChild(dropdown_input);
        dropdown_row.appendChild(dropdown_inputText);
        dropdown_row.appendChild(icon_remove);
        dropdown_row.appendChild(icon_ok);

        return dropdown_row;
    }
    static createTextInputRow_editRow(defaultText) {
        //    <div class="dropdown_inputRow">
        //        <div class="dropdown_inputText">
        //            <input type="text" size="18">
        //        </div>
        //        <div class="glyphicon glyphicon-ok dropdown_okIcons"></div>
        //        <div class="glyphicon glyphicon-remove dropdown_removeIcons"></div>
        //    </div>
        let dropdown_row = document.createElement("DIV");
        dropdown_row.classList.add("dropdown_inputRow");
        // by default its hidden
        dropdown_row.classList.toggle("hide");

        let dropdown_inputText = document.createElement("DIV");
        dropdown_inputText.classList.add("dropdown_inputText_editRow");

        let dropdown_input = document.createElement("INPUT");
        dropdown_input.setAttribute("type", "text");
        dropdown_input.setAttribute("size", "15");
        dropdown_input.value = defaultText;

        let icon_remove = document.createElement("DIV");
        icon_remove.classList.add("glyphicon", "glyphicon-remove", "dropdown_removeIcons");
        icon_remove.addEventListener("click", function() {
            Listeners.iconRemove_editRow_onclick(this, defaultText);
        });

        let icon_ok = document.createElement("DIV");
        icon_ok.classList.add("glyphicon", "glyphicon-ok", "dropdown_okIcons");
        icon_ok.addEventListener("click", function(event) {
            Listeners.iconOk_editRow_onclick(this, defaultText);
        });

        dropdown_inputText.appendChild(dropdown_input);
        dropdown_row.appendChild(dropdown_inputText);
        dropdown_row.appendChild(icon_remove);
        dropdown_row.appendChild(icon_ok);

        return dropdown_row;
    }
}