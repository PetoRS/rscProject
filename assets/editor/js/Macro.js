export default class Macro {
    static leadingZero(number) {
        let string = number + "";

        return (string.length > 1) ? string : "0" + string;
    }

    // return array -> array[0] = day
    //              -> array[1] = month
    //              -> array[2] = year
    static getToday(bool_prefix) {
        let today = new Date();
        let day = Macro.leadingZero( today.getDate() ),
            month = Macro.leadingZero( today.getMonth() + 1 ),
            year = today.getFullYear();

        return [day, month, year];
    }

    static createDateFotmatOptions() {

    }

    static createDateFormatSelect() {
        const select = document.createElement("SELECT");
        select.classList.add("form-control", "form_dateFormats");

        const today = Macro.getToday();
        let optionsText = [ today[0] + "-" + today[1] + "-" + today[2] ,
                            today[0] + "/" + today[1] + "/" + today[2] ,
                            today[0] + "." + today[1] + "." + today[2] ];

        for (let optionText of optionsText) {
            let option = document.createElement("OPTION");
            option.text = optionText;

            select.add(option);
        }

        select.addEventListener("change", function () {
            let opt = this.options[this.selectedIndex];

            this.previousSibling.previousSibling.value = opt.text;
        });

        return select;
    }

    //#DATE
    static macroRow_Date() {
        const formRow = document.createElement("DIV");
        formRow.classList.add("form_rows");

        const formGroupInline = document.createElement("DIV");
        formGroupInline.classList.add("form-group", "form-inline");

        const inputLabel = document.createElement("LABEL");
        inputLabel.classList.add("form_labels");
        inputLabel.setAttribute("for", "#DATE");
        inputLabel.innerText = "#DATE:";

        const input = document.createElement("INPUT");
        input.classList.add("form-control");
        input.type = "text";
        input.setAttribute("id", "datepicker");
        input.readOnly = true;

        const checkboxLabel = document.createElement("LABEL");
        checkboxLabel.style.marginLeft = "25px";
        checkboxLabel.classList.add("checkbox-inline");

        const checkbox = document.createElement("INPUT");
        checkbox.setAttribute("type", "checkbox");
        checkbox.checked = true;

        checkboxLabel.appendChild(checkbox);
        checkboxLabel.appendChild(document.createTextNode("Zero Prefix"));

        const select = Macro.createDateFormatSelect();

        formGroupInline.appendChild(inputLabel);
        formGroupInline.appendChild(input);
        formGroupInline.appendChild(checkboxLabel);
        formGroupInline.appendChild(select);

        formRow.appendChild(formGroupInline);

        return formRow;
    }
}
