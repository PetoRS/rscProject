/**
 * Created by Lukas on 9.8.2017.
 */
var fadeOutTimer;

function clearTimerAndShowTooltip(){
    window.clearTimeout(fadeOutTimer);
    fadeOutTimer = setTimeout(function () {
        $('#dropdown_button').tooltip('hide');
        fadeOutTimer = undefined;
    }, 3000);
    $('#dropdown_button').tooltip('show');
}
/*
 Get selection in ckeditor
 @params editor - instance of an editor
 */
function getEditorSelectedText(editor) {
    var selection = editor.getSelection();
    var selectedContent;

    if (selection.getType() == CKEDITOR.SELECTION_ELEMENT) {
        selectedContent = selection.getSelectedElement().getAttribute("alt");
    }
    else if (selection.getType() == CKEDITOR.SELECTION_TEXT) {
        selectedContent = selection.getSelectedText();
    }
    return selectedContent;
}
/*
 Get count of matching words in a text
 @params str - text for search
 word - word to search
 strict - false if you want to search not trimmed string
 */
function getMatchedWordCount(str,word,strict){
    strict = typeof strict == "boolean" ? strict : true;
    var b = strict ? '\\b' : '';
    var rex = new RegExp(b+word+b,"g");
    return str.match(rex).length;
}
/*
 Instant replace of one occurence in text
 @params editor - instance of an editor
 replacement - new word to replace
 */
function replaceOneWordOccurence(editor,replacement){
    var selection = editor.getSelection();
    var openTag = "<span>";
    var closeTag = "</span>";
    var html = openTag+selection.getSelectedText()+closeTag;
    var newElement = CKEDITOR.dom.element.createFromHtml( html, editor.document );
    newElement.setStyle('background-color','inherit');
    newElement.setStyle('color','inherit');
    newElement.setStyle('font-weight','bold');
    newElement.setText('<['+replacement+']>');
    newElement.setAttribute("contenteditable",false);
    newElement.setAttribute("readonly",true);
    addRSCToArray(newElement.getText());
    document.getElementById("openModalButton").disabled = false;
    editor.insertElement(newElement);
}

jQuery(document).ready(function ($) {
    initEditor();
    initTooltip("#dropdown_button", "Nebol označený text!", "top", "errorTooltip");

    /*
        Initialize and create a tooltip
        @params id - id for the tooltip
                desc - text for the tooltip
                place - place of the tooltip
                tooltipType - tooltip type
     */
    function initTooltip(id, desc, place, tooltipType) {
        $(id).tooltip({
            template: '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner ' + tooltipType + '"></div></div>',
            trigger: 'manual',
            placement: place,
            title: desc,
        });
    }
    /*
        Initialize editor
     */
    function initEditor() {
        CKEDITOR.replace('inputText');
        enableSaveButton();
    }
    /*
        Enable save button in ckeditor
     */
    function enableSaveButton() {
        CKEDITOR.plugins.registered['save'] = {
            init: function (editor) {
                var command = editor.addCommand('save', {
                    modes: {wysiwyg: 1, source: 1},
                    exec: function (editor) { // Add here custom function for the save button
                        $("#inputText").wordExport();
                    }
                });
                editor.ui.addButton('Save', {label: 'Uložiť', command: 'save'});
            }
        }
    }

});
export { replaceOneWordOccurence, getEditorSelectedText, getMatchedWordCount, clearTimerAndShowTooltip };


// var fadeOutTimer;
// initTooltip("#dropdown_button", "Nebol označený text!", "top", "errorTooltip");
// initEditor();

// function initEditor(){
//     tinymce.init({
//         selector: 'textarea',
//         height: 500,
//         theme: 'modern',
//         plugins: [
//             'advlist autolink lists link image charmap print preview hr anchor pagebreak',
//             'searchreplace wordcount visualblocks visualchars code fullscreen',
//             'insertdatetime media nonbreaking save table contextmenu directionality',
//             'template powerpaste textcolor colorpicker textpattern imagetools codesample toc help emoticons hr'
//         ],
//         toolbar1: 'formatselect | bold italic  strikethrough  forecolor backcolor | emoticons link media image charmap',
//         toolbar2: 'fontselect fontsizeselect | superscript subscript | alignleft aligncenter alignright alignjustify | numlist bullist outdent indent  | removeformat | code',
//         image_advtab: true,
//         templates: [
//             { title: 'Test template 1', content: 'Test 1' },
//             { title: 'Test template 2', content: 'Test 2' }
//         ],
//         content_css: [
//             '//www.tinymce.com/css/codepen.min.css',
//             'assets/css/editor.css'
//         ],
//         init_instance_callback: 'setBackgroundColor'
//     });
// }
// function setBackgroundColor(editor) {
//     editor.getBody().parentNode.style.backgroundColor = 'lightgrey';
// }
//
// function initTooltip(id, desc, place, tooltipType) {
//     $(id).tooltip({
//         template: '<div class="tooltip"><div class="tooltip-arrow ' + tooltipType + '"></div><div class="tooltip-inner ' + tooltipType + '"></div></div>',
//         trigger: 'manual',
//         placement: place,
//         title: desc,
//     });
// }
//
// document.getElementById('button_add').addEventListener('click', function (event) {
//     var editor = tinymce.get('inputText');
//     var selectedText = editor.selection.getContent({format : 'text'});
//     if (selectedText) {
//         editor.execCommand('searchreplace');
//         document.cookie = "clickedRSC=" + this.innerText;
//         console.log();
//     } else {
//         window.clearTimeout(fadeOutTimer);
//         fadeOutTimer = setTimeout(function () {
//             $('#dropdown_button').tooltip('hide');
//             fadeOutTimer = undefined;
//         }, 3000);
//         $('#dropdown_button').tooltip('show');
//     }
// });



