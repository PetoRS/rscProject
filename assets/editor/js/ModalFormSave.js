export default class ModalFormSave {

    static initModal() {
        var myRscArray = rscArray;
        console.log(myRscArray);
    }

    static saveFormAndTemplateToDatabase(rscVariablesArray) {
        var filename = document.getElementById('fileName').value;
        var editorHtmlContent = this.decodeHtml(CKEDITOR.instances['inputText'].document.getBody().getHtml());

        if (filename) {
            $.ajax({
                type: 'POST',
                data: {
                    action: "save_form_and_temp_to_database",
                    rscVariablesArray: JSON.stringify(rscVariablesArray),
                    filename: filename,
                    templateHTML: editorHtmlContent
                },
                url: 'assets/editor/php/database_functions.php',
                success: function (msg) {
                    //console.log(msg);
                    document.getElementById("cancelFileButton").click();
                }
            });
        } else {
            alert("Názov súboru nesmie byť prázdny!");
        }

    }
    static decodeHtml(html) {
        var txt = document.createElement("textarea");
        txt.innerHTML = html;
        return txt.value;
    }
}
