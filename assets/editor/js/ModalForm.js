/**
 * Created by Lukas on 31.8.2017.
 */

import Macro from './Macro';

export default class ModalForm {

    static initModal() {
        let myRscArray = rscArray;
        this.loadModal(myRscArray);
    }

    static loadModal(rscVariables) {
        const formContainer = document.getElementById('form_container');
        for (let variableName of rscVariables) {
            if (variableName === "<[#DATE]>") {
                formContainer.appendChild(Macro.macroRow_Date());

                    $("#datepicker").datepicker({
                        showOn: "button",
                        buttonImage: "http://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
                        buttonImageOnly: true,
                        buttonText: "Select date",
                        changeMonth: true,
                        changeYear: true,
                        dateFormat: 'dd-mm-yy',
                        onSelect: function (dateText, inst) {
                            alert (dateText);
                        },
                        showButtonPanel: true
                    });

                    $("#datepicker").datepicker('setDate', new Date());

                    $(".ui-datepicker-trigger").hover(
                        function() {
                            $( this ).css({'cursor' : 'pointer'});
                        }
                    );
            }
            else
                formContainer.appendChild(this.createFormRow(variableName));
        }
    }
    static resetModal(rscVariables){
        const formContainer = document.getElementById('form_container');
        rscVariables = [];
        while (formContainer.firstChild) {
            formContainer.removeChild(formContainer.firstChild);
        }
    }
    /*
        Generate form inputs
        @params rscVariables - array with rsc variables
     */
    static generateForm(rscVariables){
        let editor = CKEDITOR.instances['inputText'];
        let elements = editor.document.getBody().getElementsByTag('span');

        for ( let i = 0; i < elements.count(); i++ ) {
            let spanElement = elements.getItem(i);

            if (rscVariables.indexOf(spanElement.getText()) > -1){

                let indexOfRscVariable = rscVariables.indexOf(spanElement.getText());
                let modalFormRow = document.getElementsByClassName('form_rows')[indexOfRscVariable];
                spanElement.setText(modalFormRow.children[0].children[1].value);
                spanElement.setStyle("font-weight","inherit");
                spanElement.setStyle('background-color','inherit');
                spanElement.setStyle('color','inherit');
                spanElement.setAttribute("contentEditable",true);
            }
        }
        this.resetModal(rscVariables);
        rscArray = [];
        document.getElementById("closeFormButton").click();
        document.getElementById("openModalButton").disabled = true;
    }

    // Create form row in modal window
    // @params rscVariableName - variable name from dropdown menu
    static createFormRow(rscVariableName) {
        const formRow = document.createElement("DIV");
        formRow.classList.add("form_rows");

        const formGroupInline = document.createElement("DIV");
        formGroupInline.classList.add("form-group", "form-inline");

        const inputLabel = document.createElement("LABEL");
        inputLabel.classList.add("form_labels");
        inputLabel.setAttribute("for", rscVariableName.replace(/ /g, ''));
        inputLabel.innerText = rscVariableName.substring(2,rscVariableName.length-2) + ':';

        const input = document.createElement("INPUT");
        input.classList.add("form-control","form_inputs");
        input.type = "text";

        formGroupInline.appendChild(inputLabel);
        formGroupInline.appendChild(input);
        formRow.appendChild(formGroupInline);

        return formRow;
    }
}
