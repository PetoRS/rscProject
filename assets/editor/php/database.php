<?php

class Database
{
    public static function delete_rscCategory($conn, $rsc_categoryId, $userId) {
        $sql = "DELETE FROM rsc_variables WHERE category_id = " . $rsc_categoryId . " AND user_id = " . $userId;
        $stmt = $conn->prepare($sql);

        if (!$stmt->execute()) {
            return false;
        }

        $sql = "DELETE FROM rsc_categories WHERE id = " . $rsc_categoryId;
        $stmt = $conn->prepare($sql);

        if (!$stmt->execute()) {
            return false;
        }
    }

    public static function delete_rscVariable($conn, $rsc_variableName, $rsc_categoryId, $userId) {
        $sql = "DELETE FROM rsc_variables WHERE name = '" . $rsc_variableName ."'". " AND ". "category_id = " . $rsc_categoryId . " AND user_id = " . $userId;
        $stmt = $conn->prepare($sql);

        return $stmt->execute()?true:false;
    }

    public static function insert_rscCategory($conn, $rsc_category, $userId) {
        $sql = 'INSERT INTO rsc_categories (name, user_id) VALUES (:name, :userId)';
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':name', $rsc_category);
        $stmt->bindParam(':userId', $userId);

        return $stmt->execute()?$conn->lastInsertId():false;
    }

    public static function insert_rscVariable($conn, $rsc_variableName, $rsc_categoryId, $userId) {
        $stmt = $conn->prepare("INSERT INTO rsc_variables (name, category_id, user_id) VALUES (:name, :category_id, :user_id)");
        $stmt->bindParam(':name', $rsc_variableName);
        $stmt->bindParam(':category_id', $rsc_categoryId);
        $stmt->bindParam(':user_id', $userId);

        return $stmt->execute()?true:false;
    }

    //$conn -> PDO connection
    public static function select_rscCategories($conn, $userId) {
        $sql = "SELECT * FROM rsc_categories WHERE user_id = " . $userId;
        $stmt = $conn->prepare($sql);

        return $stmt->execute()?$stmt->fetchAll():false;
    }

    public static function select_rscVariables($conn, $userId) {
        $stmt = $conn->prepare("SELECT name, category_id FROM rsc_variables WHERE user_id = " . $userId . " ORDER BY category_id, name");

        return $stmt->execute()?$stmt->fetchAll():false;
    }

    public static function update_rscCategory($conn, $rsc_category, $rsc_categoryId, $userId) {
        $stmt = $conn->prepare("UPDATE rsc_categories SET name = :name WHERE id = :id AND user_id = :userId");
        $stmt->bindParam(':name', $rsc_category);
        $stmt->bindParam(':id', $rsc_categoryId);
        $stmt->bindParam(':userId', $userId);

        return $stmt->execute()?true:false;
    }

    public static function update_rscVariable($conn, $old_rscVariable, $new_rscVariable, $userId) {
        $stmt = $conn->prepare("UPDATE rsc_variables SET name = :new WHERE name = :old AND user_id = :userId");
        $stmt->bindParam(':old', $old_rscVariable);
        $stmt->bindParam(':new', $new_rscVariable);
        $stmt->bindParam(':userId', $userId);

        return $stmt->execute()?true:false;
    }
    public static function insert_rscFormVariables($conn, $rscVariablesString, $filename, $user_id) {
        $sql = 'INSERT INTO rsc_forms (filename, variables, user_id) VALUES (:filename, :variables, :user_id)';
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':filename', $filename);
        $stmt->bindParam(':variables', $rscVariablesString);
        $stmt->bindParam(':user_id', $user_id);

        return $stmt->execute()?$conn->lastInsertId():false;
    }
    public static function insert_rscTemplateAndVariables($conn, $rscVariablesString, $editorHtml, $user_id, $filename) {
        $sql = 'INSERT INTO rsc_templates (html, variables, filename, user_id) VALUES (:html, :variables, :filename, :user_id)';
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':html', $editorHtml);
        $stmt->bindParam(':variables', $rscVariablesString);
        $stmt->bindParam(':filename', $filename);
        $stmt->bindParam(':user_id', $user_id);

        return $stmt->execute()?$conn->lastInsertId():false;
    }
    public static function insert_rscFormAndTempId($conn, $tempId, $formId) {
        $sql = 'INSERT INTO rsc_assign (forms, templates) VALUES (:forms, :templates)';
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':forms', $formId);
        $stmt->bindParam(':templates', $tempId);

        return $stmt->execute()?true:false;
    }
}