<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//DOMPDF
//require_once __DIR__ . '/../../php/plugins/dompdf_0-8-2/dompdf/autoload.inc.php';
//
//use Dompdf\Dompdf;
//
//if (isset($_POST['html'])) {
//    $dompdf = new DOMPDF();
//
//    $html = $_POST['html'];
//    var_dump($html);
//    $html = mb_convert_encoding($html, 'HTML-ENTITIES','UTF-8');
//    $dompdf->loadHtml($html,"UTF-8");
//    $dompdf->setPaper('A4', 'portrait');
//    $dompdf->render();
//    $output = $dompdf->output();
//    file_put_contents("new-file-" . time() . ".pdf", $output);
//    exit(0);
//}

//MPDF

if (isset($_POST['html'])){
    $html = $_POST['html'];
    $html = htmlspecialchars_decode($html);

    include __DIR__ . '/../../php/plugins/MPDF_6_0/mpdf60/mpdf.php';
    $mpdf = new mPDF("utf-8","A4");
    $mpdf->allow_charset_conversion = true;
    $mpdf->charset_in = 'UTF-8';
    $mpdf->WriteHTML($html);
    $mpdf->Output("newfile-".time().".pdf",'F');

    exit(0);
}

