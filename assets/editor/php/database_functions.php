<?php
require_once __DIR__ . '/../../../assets/global/php/config.php';
require_once 'database.php';

session_start();

if ( isset($_SESSION['userId']) ) {
    $userId = $_SESSION['userId'];

    if ( $userId != "1") {
        if ($_POST["action"] == "select_dropdownCategories") {
            $json = Database::select_rscCategories($pdo, $userId);
            if ( !empty($json) )
                echo json_encode($json);
            else
                echo json_encode(json_decode("{}"));
        }

        if ($_POST["action"] == "select_dropdownRows") {
            $json = Database::select_rscVariables($pdo, $userId);

            if ($json) {
                echo json_encode($json);
            } else
                echo json_encode(json_decode("{}"));
        }

        if ($_POST["action"] == "delete_dropdownRow") {
            $rsc_variableName = $_POST["rowName"];
            $rsc_categoryId = $_POST["categoryId"];

            $status = Database::delete_rscVariable($pdo, $rsc_variableName, $rsc_categoryId, $userId);

            echo $status;
        }

        if ($_POST["action"] == "insert_dropdownRow") {
            $rsc_variableName = $_POST["rowName"];
            $rsc_categoryId = $_POST["categoryId"];

            $status = Database::insert_rscVariable($pdo, $rsc_variableName, $rsc_categoryId, $userId);

            echo $status;
        }

        if ($_POST["action"] == "update_dropdownCategoryName") {
            $rsc_categoryName = $_POST["categoryName"];
            $rsc_categoryId = &$_POST["categoryId"];

            $status = Database::update_rscCategory($pdo, $rsc_categoryName, $rsc_categoryId, $userId);

            echo $status;
        }

        if ($_POST["action"] == "insert_dropdownCategoryName") {
            $rsc_categoryName = $_POST["categoryName"];

            $status = Database::insert_rscCategory($pdo, $rsc_categoryName, $userId);

            echo $status;
        }

        if ($_POST["action"] == "delete_dropdownCategory") {
            $rsc_categoryId = $_POST["categoryId"];

            $status = Database::delete_rscCategory($pdo, $rsc_categoryId, $userId);

            echo $status;
        }

        if ($_POST["action"] == "update_dropdownRow") {
            $oldRowName = $_POST["oldRowName"];
            $newRowName = $_POST["newRowName"];

            $status = Database::update_rscVariable($pdo, $oldRowName, $newRowName, $userId);

            echo $status;
        }

        if ($_POST["action"] == "save_form_and_temp_to_database") {
            $rscVariablesArray = json_decode($_POST["rscVariablesArray"]);
            $filename = $_POST["filename"];
            $editorHtml = $_POST["templateHTML"];
            $rscString = implode(";",$rscVariablesArray);
            $lastFromId = Database::insert_rscFormVariables($pdo,$rscString, $filename, $userId);
            $lastTemplateId = Database::insert_rscTemplateAndVariables($pdo, $rscString, $editorHtml, $userId, $filename);
            Database::insert_rscFormAndTempId($pdo, $lastTemplateId, $lastFromId);
        }
    }
    else {
        echo json_encode(json_decode("{}"));
    }
}
