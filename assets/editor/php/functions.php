<?php
function array_union($array1, $array2) {
    return array_values( array_unique( array_merge($array1, $array2)));
}