import Database from './js/Database';
import ModalForm from './js/ModalForm';
import ModalFormSave from './js/ModalFormSave';

/* When the user clicks on the dropdown button, dropdown container toggles between hide/show */
document.getElementById("dropdown_button").addEventListener("click", function() {
    document.getElementById("dropdown_container").classList.toggle("show");
});

document.getElementById("exportToPdfButton").addEventListener("click", function() {
    $.ajax({
        type: 'POST',
        data: {html: CKEDITOR.instances['inputText'].document.getBody().getHtml()},
        url: 'assets/editor/php/exportToPdf.php',
        success: function (msg) {
            console.log(msg);
            alert("PDF file has been created!");
        },
        error: function(request, status, error) {
            alert("Error exporting to PDF file!");
        }
    });
});


document.getElementById("openModalButton").disabled = true;
document.getElementById('openModalButton').addEventListener("click",function(event){
    ModalForm.initModal();
});
document.getElementById("closeFormButton").addEventListener("click", function (event) {
    ModalForm.resetModal(rscArray);
});
document.getElementById("acceptFormButton").addEventListener("click", function (event) {
    ModalForm.generateForm(rscArray);
});
document.getElementById("saveToDbFormButton").addEventListener("click", function (event) {
    ModalFormSave.initModal();
});
document.getElementById("saveFileButton").addEventListener("click", function (event) {
    ModalFormSave.saveFormAndTemplateToDatabase(rscArray);
});

document.getElementById("logOut").addEventListener("click", function () {
    $.ajax({
        type: 'POST',
        data: {action: "logOut"},
        url: 'assets/login/php/actions.php',
        success: function (msg) {
            window.location = "http://rsc.sidlo.sro.sk/rscProject_PH/login.php";
        }
    });
});
window.onload = function () {
    Database.select_dropdownCategories();
};

