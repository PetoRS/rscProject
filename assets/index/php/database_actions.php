<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

require_once __DIR__ . '/../../../assets/global/php/config.php';
require_once 'Database.php';

$action = $_POST["action"];

if ( isset($_SESSION['userId']) ) {
    $userId = $_SESSION['userId'];

    if ( $userId != "1") {
        if ($action == "delete_form") {
            $formId = $_POST["formId"];
            Database::delete_form($pdo, $formId, $userId);
        }
        else if ($action == "delete_template") {
            $templateId = $_POST["templateId"];
            Database::delete_template($pdo, $templateId, $userId);
        }
        else if ($action == "select_forms") {
            $json = Database::select_forms($pdo, $userId);
            if ( !empty($json) )
                echo json_encode($json);
            else
                echo json_encode(json_decode("{}"));
        }

        else if ($action == "select_templates") {
            $json = Database::select_templates($pdo, $userId);
            if ( !empty($json) ) {
                echo json_encode($json);
            }
            else
                echo json_encode(json_decode("{}"));
        }
    }
    else {
        echo json_encode(json_decode("{}"));
    }
}
