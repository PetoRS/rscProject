<?php

class Database
{
    public static function delete_form($conn, $form_id, $userId) {
        $sql = "DELETE FROM rsc_assign WHERE forms = " . $form_id;
        $stmt = $conn->prepare($sql);

        if (!$stmt->execute()) {
            return false;
        }

        $sql = "DELETE FROM rsc_forms WHERE id = " . $form_id . " AND user_id = " . $userId;
        $stmt = $conn->prepare($sql);

        if (!$stmt->execute()) {
            return false;
        }
    }

    public static function delete_template($conn, $template_id, $userId) {
        $sql = "DELETE FROM rsc_assign WHERE templates = " . $template_id;
        $stmt = $conn->prepare($sql);

        if (!$stmt->execute()) {
            return false;
        }

        $sql = "DELETE FROM rsc_templates WHERE id = " . $template_id . " AND user_id = " . $userId;
        $stmt = $conn->prepare($sql);

        if (!$stmt->execute()) {
            return false;
        }
    }

    public static function select_forms($conn, $userId) {
        $sql = "SELECT * FROM rsc_forms WHERE user_id = " . $userId;
        $stmt = $conn->prepare($sql);

        return $stmt->execute()?$stmt->fetchAll():false;
    }

    public static function select_templates($conn, $userId) {
        $sql = "SELECT * FROM rsc_templates WHERE user_id = " . $userId;
        $stmt = $conn->prepare($sql);

        return $stmt->execute()?$stmt->fetchAll():false;
    }
}