import Listeners from './js/Listeners';
import Superform from './js/Superform';
import TableForms from './js/TableForms';
import TableTemplates from './js/TableTemplates';

///////////////////////////////////////////////////////////////////////////
// import React from 'react';
// import ReactDOM from 'react-dom';
// import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
// import RaisedButtonExampleSimple from './components/test.jsx';

window.onload = function () {
    document.getElementById("logOut").addEventListener("click", function () {
        $.ajax({
            type: 'POST',
            data: {action: "logOut"},
            url: 'assets/login/php/actions.php',
            success: function () {
                window.location = "http://rsc.sidlo.sro.sk/rscProject_PH/login.php";
            }
        });
    });

    document.getElementById("button_superform").addEventListener("click", Superform.createSuperform);

    document.getElementById("button_downloadPdf").addEventListener("click", Listeners.downloadPdfs);

    TableForms.loadFormsTable();
    TableTemplates.loadFormsTable();
    document.getElementById("checkbox_formsRows").addEventListener("click", Listeners.table_head_clicked);
    document.getElementById("checkbox_formsTemplates").addEventListener("click", Listeners.table_head_clicked);

    //Modal listeners
    const modals = document.getElementsByClassName("modal");
    for (let i=0; i<modals.length; i++) {
        if (modals[i].id === "modal_superform") {
            $(modals[i]).on("hidden.bs.modal", function () {
                modals[i].querySelector("#form_container").innerHTML = "";
            });
        }
        if (modals[i].id === "modal_templates") {
            $(modals[i]).on("hidden.bs.modal", function () {
                modals[i].querySelector(".modalDiv_htmlPreview").innerHTML = "";
            });
        }
    }

    // const App = () => (
    //     <MuiThemeProvider>
    //         <RaisedButtonExampleSimple />
    //     </MuiThemeProvider>
    // );
    //
    // ReactDOM.render(
    //     <App />,
    //     document.getElementById('button_superform')
    // );
};
