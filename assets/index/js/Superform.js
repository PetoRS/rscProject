import ModalForm from '../../editor/js/ModalForm';

export default class Superform {
    static createSuperform() {
        const htmlCol_clickedRows = document.getElementById("table_templates").querySelectorAll("tbody input[type=checkbox]:checked");

        let str_variables;
        let array = [];
        let set = new Set();
        for (let i=0; i<htmlCol_clickedRows.length; i++) {
            str_variables = htmlCol_clickedRows[i].parentNode.parentNode.dataset["variables"];
            array = str_variables.split(";");
            for (let j=0; j<array.length; j++) {
                set.add(array[j]);
            }
        }
        array = Array.from(set);

        ModalForm.loadModal(array);
        $("#modal_superform").modal("toggle");
    }
}