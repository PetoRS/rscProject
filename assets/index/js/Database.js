import TableForms from './TableForms';
import TableTemplates from "./TableTemplates";

export default class Database {
    static delete_form(form_id) {
        $.ajax({
            type: 'POST',
            data: {action: "delete_form",
                   formId: form_id},
            url: 'assets/index/php/database_actions.php',
            success: function (msg) {
            }
        });
    }
    static delete_template(template_id) {
        $.ajax({
            type: 'POST',
            data: {action: "delete_template",
                   templateId: template_id},
            url: 'assets/index/php/database_actions.php',
            success: function (msg) {
            }
        });
    }
    static select_forms() {
        $.ajax({
            type: 'POST',
            data: {action: "select_forms"},
            url: 'assets/index/php/database_actions.php',
            success: function (msg) {
                const tbody = document.getElementById("table_forms").getElementsByTagName("TBODY")[0];
                const json = JSON.parse(msg);

                TableForms.appendTableRows(tbody, json);
            }
        });
    }

    static select_templates() {
        $.ajax({
            type: 'POST',
            data: {action: "select_templates"},
            url: 'assets/index/php/database_actions.php',
            success: function (msg) {
                const tbody = document.getElementById("table_templates").getElementsByTagName("TBODY")[0];
                const json = JSON.parse(msg);

                TableTemplates.appendTableRows(tbody, json);
            }
        });
    }
}