import Database from './Database';
import Listeners from './Listeners';

export default class TableForms {
    static loadFormsTable() {
        Database.select_forms();
    }

    static appendTableRows(tbody, json) {

        for (let i=0; i<json.length; i++) {
            let tr = document.createElement("TR");
            tr.setAttribute("data-id", json[i]["id"]);
            tr.setAttribute("data-variables", json[i]["variables"]);

            let td1 = document.createElement("TD"),
                td2 = document.createElement("TD"),
                td3 = document.createElement("TD"),
                td4 = document.createElement("TD"),
                td5 = document.createElement("TD");

            td1.classList.add("table_forms_narrow");
            td2.classList.add("table_forms_narrow");
            td4.classList.add("table_forms_narrow");
            td5.classList.add("table_forms_narrow");

            td1.innerText = i + 1;

            let checkbox = document.createElement("INPUT");
            checkbox.setAttribute("type", "checkbox");
            checkbox.addEventListener("click", Listeners.table_rows_clicked);
            td2.appendChild(checkbox);

            let a = document.createElement("A");
            a.innerText = json[i]["filename"];
            a.setAttribute("href", "#");
            a.addEventListener("click", function () {
                const rscDiv = document.getElementById("modal_forms").querySelector(".modalDiv_rscVariables");
                rscDiv.innerText = this.parentNode.parentNode.dataset["variables"].replace(/<\[/g, " ").replace(/\]>/g, "").replace(/;/g, ",");
                $("#modal_forms").modal("toggle");
            });

            td3.appendChild(a);

            td4.innerText = json[i]["variables"].split(";").length;

            let div_removIcon = document.createElement("DIV");
            div_removIcon.classList.add("glyphicon", "glyphicon-trash", "dropdown_trashIcons");
            div_removIcon.addEventListener("click", Listeners.removeIconForm_onclick);

            td5.appendChild(div_removIcon);

            tr.appendChild(td1);
            tr.appendChild(td2);
            tr.appendChild(td3);
            tr.appendChild(td4);
            tr.appendChild(td5);

            tbody.appendChild(tr);
        }
    }
}