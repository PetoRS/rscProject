import Database from './Database.js';
import ModalForm from '../../editor/js/ModalForm';

export default class Listeners {
    static removeIconTemplate_onclick() {
        let tr = this.parentNode.parentNode;
        tr.parentNode.removeChild(tr);

        Database.delete_template(tr.dataset["id"]);
    }

    static removeIconForm_onclick() {
        let tr = this.parentNode.parentNode;
        tr.parentNode.removeChild(tr);

        Database.delete_form(tr.dataset["id"]);
    }

    static table_head_clicked() {
        const table = this.parentNode.parentNode.parentNode.parentNode;
        const table_rows = table.querySelectorAll("tbody tr");

        if (this.checked) {
            for (let i = 0; i < table_rows.length; i++) {
                table_rows[i].classList.add("tr_selected");
                table_rows[i].children[1].firstChild.checked = true;
            }
        }
        else {
            for (let i = 0; i < table_rows.length; i++) {
                table_rows[i].classList.remove("tr_selected");
                table_rows[i].children[1].firstChild.checked = false;
            }
        }
    }

    static table_rows_clicked() {
        if (this.checked)
            this.parentNode.parentNode.classList.add("tr_selected");
        else
            this.parentNode.parentNode.classList.remove("tr_selected");
    }

    static downloadPdfs() {
        const htmlCol_clickedRows = document.getElementById("table_templates").querySelectorAll("tbody input[type=checkbox]:checked");

        let str_variables;
        let array = [];
        let htmls = [];
        let set = new Set();
        for (let i = 0; i < htmlCol_clickedRows.length; i++) {
            str_variables = htmlCol_clickedRows[i].parentNode.parentNode.dataset["variables"];
            array = str_variables.split(";");

            htmls.push(htmlCol_clickedRows[i].parentNode.parentNode.dataset["html"]);
            for (let j = 0; j < array.length; j++) {
                set.add(array[j]);
            }
        }
        array = Array.from(set);

        let el_superformInputs = document.getElementById("modal_superform").querySelectorAll("input[type=text]");

        let str_inputValue = null;
        let str_label = null;
        for (let i=0; i<el_superformInputs.length; i++) {
            str_label = el_superformInputs[i].previousSibling.attributes["for"].value;
            str_inputValue = el_superformInputs[i].value;
            for (let j=0; j<htmls.length; j++) {
                if (htmls[j].indexOf(str_label) !== -1) {
                    console.log(str_label);
                    console.log(str_inputValue);
                    htmls[j] = htmls[j].replace(str_label, str_inputValue)
                    console.log(htmls[j]);
                }
            }
        }
    }
}