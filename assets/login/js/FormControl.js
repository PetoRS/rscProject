//import './formControlFunctions';
import * as formControlFunctions from "./formControlFunctions";

export default class FormControl {
    static database_checkEmail(email) {
        alert(formControlFunctions.usernameCheck);
        $.ajax({
            type: 'POST',
            data: {action: "checkEmail",
                email: email},
            url: 'assets/login/php/actions.php',
            success: function (msg) {
                if (msg)
                    emailCheck = true;
                else
                    emailCheck = false;
                formControlFunctions.checkIfEnableRegisterButton();
            }
        });
    }
    
    static database_checkUsername(username) {
        $.ajax({
            type: 'POST',
            data: {action: "checkUsername",
                   username: username},
            url: 'assets/login/php/actions.php',
            success: function (msg) {
                if (msg)
                    formControlFunctions.usernameCheck = true;
                else
                    formControlFunctions.usernameCheck = false;
                formControlFunctions.checkIfEnableRegisterButton();
            }
        });
    }
}