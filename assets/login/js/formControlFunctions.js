var usernameCheck = false;
var emailCheck = false;
var passwordCheck = false;
var retypePasswordCheck = false;

/*
 * Form control listeners
 */
$( function () {
    $('#form_username').tooltip({
        container: 'body',
        placement: 'right',
        template: '<div class="tooltip">' +
        '<div class="tooltip-arrow">' +
        '</div>' +
        '<div class="tooltip-inner">' +
        '</div>',
        trigger: 'manual'
    });
    $('#form_email').tooltip({
        container: 'body',
        placement: 'right',
        template: '<div class="tooltip">' +
        '<div class="tooltip-arrow">' +
        '</div>' +
        '<div class="tooltip-inner">' +
        '</div>',
        trigger: 'manual'
    });
    $('#form_password').tooltip({
        container: 'body',
        placement: 'right',
        template: '<div class="tooltip">' +
        '<div class="tooltip-arrow">' +
        '</div>' +
        '<div class="tooltip-inner">' +
        '</div>',
        trigger: 'manual'
    });
    $('#form_retypePassword').tooltip({
        container: 'body',
        placement: 'right',
        template: '<div class="tooltip">' +
        '<div class="tooltip-arrow">' +
        '</div>' +
        '<div class="tooltip-inner">' +
        '</div>',
        trigger: 'manual'
    });
});

function initTooltip(id, message) {
    id = "#" + id;
    $(id).attr('title', message);
    $(id).tooltip('fixTitle');
}

function checkIfEnableRegisterButton() {
    let result = usernameCheck && emailCheck && passwordCheck && retypePasswordCheck;

    if (result)
        document.getElementById("registerButton").disabled = false;
    else
        document.getElementById("registerButton").disabled = true;
}


document.getElementById("form_username").addEventListener("blur", function () {
    const element = this;
    if ( element.value === "" ) {
        usernameCheck = false;

        initTooltip("form_username", "Username field empty");
        $('#form_username').tooltip('show');

        return;
    }

    $.ajax({
        type: 'POST',
        data: {action: "checkUsername",
            username: this.value},
        url: 'assets/login/php/actions.php',
        success: function (msg) {
            if (msg) {
                usernameCheck = true;

                $('#form_username').tooltip('hide');
            }
            else {
                usernameCheck = false;

                initTooltip("form_username", "Username already used");
                $('#form_username').tooltip('show');
            }


            checkIfEnableRegisterButton();
        }
    });
});

document.getElementById("form_email").addEventListener("blur", function () {
    const element = this;
    if ( element.value === "" ) {
        emailCheck = false;

        initTooltip("form_email", "Email field empty");
        $('#form_email').tooltip('show');

        return;
    }

    $.ajax({
        type: 'POST',
        data: {action: "checkEmail",
            email: this.value},
        url: 'assets/login/php/actions.php',
        success: function (msg) {
            if (msg) {
                emailCheck = true;

                $('#form_email').tooltip('hide');
            }
            else {
                emailCheck = false;

                initTooltip("form_email", "Email already used");
                $('#form_email').tooltip('show');
            }

            checkIfEnableRegisterButton();
        }
    });

});

document.getElementById("form_password").addEventListener("blur", function () {
    const element = this;
    if ( element.value === "" ) {
        passwordCheck = false;

        initTooltip("form_password", "Password field empty");
        $('#form_password').tooltip('show');
    }

    else if ( element.value.length < 4) {
        passwordCheck = false;

        initTooltip("form_password", "Password not long enough");
        $('#form_password').tooltip('show');
    }

    else {
        if (document.getElementById("form_retypePassword").value !== "") {
            if (document.getElementById("form_retypePassword").value !== element.value) {
                retypePasswordCheck = false;

                initTooltip("form_retypePassword", "Passwords don´t match");
                $('#form_retypePassword').tooltip('show');
            }
            else {
                retypePasswordCheck = true;
                $('#form_retypePassword').tooltip('hide');
            }
        }
        else {
            passwordCheck = true;
            $('#form_password').tooltip('hide');
        }
    }

    checkIfEnableRegisterButton();
});

document.getElementById("form_retypePassword").addEventListener("blur", function () {
    const element = this;

    if ( element.value === "" ) {
        retypePasswordCheck = false;

        initTooltip("form_retypePassword", "Re-type password field empty");
        $('#form_retypePassword').tooltip('show');
    }
    else {
        if (element.value !== document.getElementById("form_password").value) {
            retypePasswordCheck = false;

            initTooltip("form_retypePassword", "Passwords don´t match");
            $('#form_retypePassword').tooltip('show');
        }
        else {
            retypePasswordCheck = true;

            $('#form_retypePassword').tooltip('hide');
        }
    }

    checkIfEnableRegisterButton();
});