import './formControlFunctions';

document.getElementById("redirectToEditor").addEventListener("click", function () {
    $.ajax({
        type: 'POST',
        data: {action: "freeUser"},
        url: 'assets/login/php/actions.php',
        success: function (msg) {
            //window.location = "http://rsc.sidlo.sro.sk/rscProject/editor.php";
            window.location = "http://rsc.sidlo.sro.sk/rscProject_PH/editor.php";
        }
    });
});

//onclick hide "login container" and display back "register container"
document.getElementById("redirectToRegister").addEventListener("click", function () {
    document.getElementById("login_logInDiv").style.display = "none";
    document.getElementById("login_registerDiv").style.display = "block";
});

//onclick hide "register container" and display back "login container"
document.getElementById("redirectToLogin").addEventListener("click", function () {
    document.getElementById("login_registerDiv").style.display = "none";
    document.getElementById("login_logInDiv").style.display = "block";
});

