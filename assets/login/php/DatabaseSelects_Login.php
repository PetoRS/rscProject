<?php

class DatabaseSelects_Login
{
    public static function insert_user($conn, $username, $email, $name, $surname, $password) {
        try {
            $sql = "INSERT INTO rsc_users (username, email, name, surname, password) VALUES (:username, :email, :name, :surname, :password)";
            $stmt = $conn->prepare($sql);

            $stmt->bindParam(':username', $username);
            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':surname', $surname);
            $stmt->bindParam(':password', $password);

            return $stmt->execute() ? true : false;
        }
        catch (PDOException $e) {
            echo "Error in function: insert_user" . $e->getMessage();
        }
    }

    public static function select_byEmail($conn, $email) {
        try {
            $sql = "SELECT * from rsc_users WHERE email = '" . $email . "'";
            $stmt = $conn->prepare($sql);

            return $stmt->execute() ? $stmt->fetchAll() : false;
        }
        catch (PDOException $e) {
            echo "Error in function: select_byEmail" . $e->getMessage();
        }
    }

    public static function select_passwordByLogin($conn, $login) {
        try {
            $sql = "SELECT id, username, password from rsc_users WHERE username = '" . $login . "' OR email = '" . $login . "'";
            $stmt = $conn->prepare($sql);

            return $stmt->execute() ? $stmt->fetchAll() : false;
        }
        catch (PDOException $e) {
            echo "Error in function: select_passwordByLogin" . $e->getMessage();
        }
    }

    public static function select_byUsername($conn, $username) {
        try {
            $sql = "SELECT * from rsc_users WHERE username = '" . $username . "'";
            $stmt = $conn->prepare($sql);

            return $stmt->execute() ? $stmt->fetchAll() : false;
        }
        catch (PDOException $e) {
            echo "Error in function: select_byUsername" . $e->getMessage();
        }
    }
}