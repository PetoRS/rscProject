<?php

require_once 'DatabaseSelects_Login.php';

/* Function for user authentication
 * RETURN VALUE [BOOLEAN]: "true" - if correct credentials
 *                         "false" - if incorrect credentials
 */
function logInAuthentication($pdo, $login, $password) {
    //hashed password from database
    $credentials = DatabaseSelects_Login::select_passwordByLogin($pdo, $login);

    if ( password_verify($password, $credentials[0]["password"]) ) {

        return $credentials;
    }
}