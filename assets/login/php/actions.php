<?php
session_start();

require_once __DIR__ . '/../../../assets/global/php/config.php';
require_once 'functions.php';

$action = $_POST["action"];

if ( $action == "logIn") {
    $login = $_POST["login"];
    $password = $_POST["password"];

    $credentials = logInAuthentication($pdo, $login, $password);

    if ( !empty($credentials) ) {

        $_SESSION['userId'] = $credentials[0]["id"];
        $_SESSION['username'] = $credentials[0]["username"];

        header('Location: http://rsc.sidlo.sro.sk/rscProject_PH/index.php');
        exit();
    }
    else
        echo "<DIV class='login_status'>Wrong username or password</DIV>";
}

else if ( $action == "logOut") {
    $_SESSION = array();
}

else if ( $action == "register"  ) {
    $username = $_POST["username"];
    $email = $_POST["email"];
    $name = $_POST["name"];
    $surname = $_POST["surname"];
    $password = $_POST["password"];

    //password hashing
    $password = password_hash($password, PASSWORD_DEFAULT);

    //insert into database
    DatabaseSelects_Login::insert_user($pdo, $username, $email, $name, $surname, $password);

    header('Location: http://www.rsc.sidlo.sro.sk/rscProject_PH/index.php');
}

else if ( $action == "freeUser") {
    $_SESSION['userId'] = "1";
    $_SESSION['username'] = "Free User";
}

else if ( $action == "checkUsername" ) {
    $username = $_POST["username"];

    $status = DatabaseSelects_Login::select_byUsername($pdo, $username);

    if ( empty($status) )
        echo true;
    else
        echo false;
}

else if ( $action == "checkEmail" ) {
    $email = $_POST["email"];

    $status = DatabaseSelects_Login::select_byEmail($pdo, $email);

    if ( empty($status) )
        echo true;
    else
        echo false;
}