/**
 * Created by Lukas on 23.8.2017.
 */
// Array for rsc variables
var rscArray = [];
/*
    Get cookie from cookies
    @params name - cookie name
 */
function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}
/*
    Set replaceWith field value
 */
function setReplaceWithField() {
    var inputReplaceWith = document.getElementsByClassName('cke_dialog_ui_input_text');
    inputReplaceWith[5].value = '<[' + getCookie("clickedRSC") + ']>';
    inputReplaceWith[5].readOnly = true;
}
/*
    Get replaceWith field value
 */
function getReplaceWithFieldValue(){
    var inputReplaceWith = document.getElementsByClassName('cke_dialog_ui_input_text');
    return inputReplaceWith[5].value;
}
/*
    Get find input value
 */
function getFindFieldValue(){
    var inputReplaceWith = document.getElementsByClassName('cke_dialog_ui_input_text');
    return inputReplaceWith[3].value;
}
/*
    Add new variable to array
    @params rscName - rsc variable name
 */
function addRSCToArray(rscName){
    if (!isInArray(rscName,rscArray))
        rscArray.push(rscName);
}
/*
    Check if rsc is in rscArray
    @params value - rsc variable
            array - rsc array with variables
 */
function isInArray(value, array) {
    return array.indexOf(value) > -1;
}
/*
    Remove highlighs from text
 */
function removeHighlights(){
    var editor = CKEDITOR.instances['inputText'];
    var elements = editor.document.getBody().getElementsByTag('span');
    for ( var i = 0; i < elements.count(); i++ ) {
        var textToReplace = getReplaceWithFieldValue();
        var textFind = getFindFieldValue();
        var spanElement = elements.getItem(i);
        if (spanElement.getText() === textToReplace){
            spanElement.setStyle('background-color','inherit');
            spanElement.setStyle('color','inherit');
        }
        if (spanElement.getText() === textFind){
            spanElement.setStyle('background-color','inherit');
            spanElement.setStyle('color','inherit');
        }
        if (spanElement.getName() === "span" && spanElement.getText() === textToReplace){
            spanElement.setStyle('font-weight','bold');
            spanElement.setAttribute("contentEditable",false);
            spanElement.setAttribute("readonly",true);
        }
    }
}

