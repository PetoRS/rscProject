/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    config.toolbar = [
        {
            name: 'clipboard',
            groups: ['clipboard', 'undo'],
            items: ['Save','Print', 'Undo', 'Redo', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-']
        },
        {name: 'editing', groups: ['find', 'selection', 'spellchecker'], items: ['Scayt']},
        {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
        {name: 'insert', items: ['Image', 'Table', 'HorizontalRule', 'SpecialChar']},
        {name: 'tools', items: ['Maximize']},
        {name: 'others', items: ['-']},
        {
            name: 'paragraph',
            groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
            items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote','Smiley']
        },
        {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source']},
        {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize','TextColor', 'BGColor']},
        {
            name: 'basicstyles',
            groups: ['basicstyles', 'cleanup'],
            items: ['Bold', 'Italic', 'Underline', 'Strike','RemoveFormat','JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
        },
        {name: 'about', items: ['About']}
    ];
    config.allowedContent = true;

    config.extraPlugins = 'simpleruler';
    config.extraPlugins = 'pastefromword';
    config.extraPlugins = 'colorbutton';
    config.extraPlugins = 'save';
    config.resize_enabled = false;

    config.height = '1400px';
    config.skin = 'moono';

};
