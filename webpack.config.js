module.exports = {
    entry: {
        index: __dirname + '/assets/index/indexApp.js',
        editor: __dirname + '/assets/editor/editorApp.js',
        login: __dirname + '/assets/login/loginApp.js'
    },
    output: {
        filename: '[name]Bundle.js',
        path: __dirname + '/dist'
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader" }
                ]
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.jsx$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.scss$/,
                use: [
                    { loader: "style-loader" },
                    { loader: "sass-loader" }
                ]
            }
        ]
    },
    devServer: {
        contentBase: __dirname + '/dist',
        port: 9000
    }
};