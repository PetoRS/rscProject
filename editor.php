<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

if ( !isset($_SESSION["userId"]) ) {
    header('Location: http://rsc.sidlo.sro.sk/rscProject_PH/login.php');
    exit();
}
?>
<!DOCTYPE html>
<html lang="sk">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="assets/editor/css/dropdown.css">
    <link rel="stylesheet" href="assets/editor/css/editor.css">
    <link rel="stylesheet" href="assets/editor/css/modalForm.css">
    <title>Projekt RSC</title>
</head>
<body>
<div class="wrapper">
    <ul class="navigationBar">
        <li class="navigationBar_items"><a href="http://rsc.sidlo.sro.sk/rscProject_PH/index.php">Index</a></li>
        <li class="navigationBar_items"><a href="http://rsc.sidlo.sro.sk/rscProject_PH/editor.php">Editor</a></li>
        <li class="navigationBar_items"><a href=""></a></li>
        <li id="logOut" class="navigationBar_items loggedUser">
            <a>
                <span class="glyphicon glyphicon-log-out icons"></span>
                <?php if (isset($_SESSION["userId"]) ) echo ($_SESSION["userId"] == "1")?"Log In":"Log Out" ?>
            </a>
        </li>
        <li class="navigationBar_items rightFloat">
            <a>
                <span class="glyphicon glyphicon-user icons">
                </span>
                <?php if (isset($_SESSION["username"]) ) echo $_SESSION["username"];?>
            </a>
        </li>
    </ul>
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <div id="dropdown" class="dropdown">
                    <button type="button" class="dropdown_button" id="dropdown_button">Choose RSC
                        <span class="caret"></span>
                    </button>
                    <div id="dropdown_container" class="dropdown_container">
                    </div>
                </div>
            </div>
            <!-- Trigger the modal with a button -->
            <div class="col-md-6">
                <button id="openModalButton" type="button" class="btn btn-default" data-toggle="modal"
                        data-target="#myModal">Generovať formulár
                </button>
                <button id="exportToPdfButton" type="button" class="btn btn-default">Exportovať do PDF</button>
                <!--                <button class="btn btn-default" type="button" id="button_add" value="ADD RSC">ADD RSC</button>-->
                <!--                <button class="btn btn-default" type="button" id="button_add2" value="ADD RSC 2">Meno</button>-->
                <!--                <button class="btn btn-default" type="button" id="button_add3" value="ADD RSC 3">Priezvisko</button>-->
            </div>
            <form method="post">
                <div class="col-md-12" id="editor_div">
                    <textarea title="ckeditor" id="inputText">
                        <p style="margin: 0cm 0cm 10pt; text-align: center;"><span
                                    style="font-family:Times New Roman,Times,serif;"><span style="color:#c0392b;"><span
                                            new="" roman="" times=""><span style="font-size: 11pt;"><span
                                                    style="line-height: normal;"><b><span style="font-size: 24pt;"><span
                                                                new="" roman="" times=""><span
                                                                    style="background-color:#f1c40f;">Registr&aacute;cia predmetov na ak. rok 2017-2018</span></span></span></b></span></span></span></span></span></p>

                        <p align="center" style="margin-bottom:.0001pt; text-align:center; margin:0cm 0cm 10pt"><span
                                    style="font-family:Times New Roman,Times,serif;"><span style="font-size:11pt"><span
                                            style="line-height:normal"><b><span style="font-size:12.0pt"><span new=""
                                                                                                               roman=""
                                                                                                               times="">Povinn&aacute; pre v&scaron;etk&yacute;ch &scaron;tudentov</span></span></b><span
                                                style="font-size:12.0pt"><span new="" roman="" times=""> <b>od 18.5.2017 do 30.6.2017</b></span></span></span></span></span></p>

                        <p style="margin-bottom:.0001pt; margin:0cm 0cm 10pt">&nbsp;</p>

                        <p style="margin: 0cm 0cm 10pt;"><span style="font-family:Times New Roman,Times,serif;"><span
                                        style="font-size:11pt"><span style="line-height:normal"><span
                                                style="font-size:12.0pt"><span new="" roman="" times="">K registr&aacute;cii pristupujte zodpovedne, pretože <b>zmena predmetov pri z&aacute;pise už nebude možn&aacute;.</b></span></span></span></span></span></p>

                        <p style="margin: 0cm 0cm 10pt;"><span style="font-family:Times New Roman,Times,serif;"><span
                                        style="font-size:11pt"><span style="line-height:normal"><span
                                                style="font-size:12.0pt"><span new="" roman="" times="">&Scaron;tudijn&yacute; program FEI STU na ak. rok 2017/2018 je pre &scaron;tudentov pr&iacute;stupn&yacute; na </span></span><a
                                                href="http://www.fei.stuba.sk/docs/2017/SP2017_2018.pdf"
                                                style="color:blue; text-decoration:underline"><span
                                                    style="font-size:12.0pt"><span new="" roman="" times="">http://www.fei.stuba.sk/docs//2017/SP2017_2018.pdf</span></span></a> <span
                                                style="font-size:12.0pt"><span new="" roman="" times="">tak, aby s n&iacute;m mohli &scaron;tudenti akt&iacute;vne pracovať pri tvorbe svojich &scaron;tudijn&yacute;ch pl&aacute;nov.</span></span></span></span></span></p>

                        <p style="margin: 0cm 0cm 10pt;"><span style="font-family:Times New Roman,Times,serif;"><span
                                        style="font-size:11pt"><span style="line-height:12.0pt"><u><span
                                                    style="font-size:12.0pt"><span new="" roman="" times="">D&ocirc;ležit&eacute; na preč&iacute;tanie v &scaron;tudijnom programe pre &scaron;tudentov bakal&aacute;rskeho &scaron;t&uacute;dia</span></span></u><span
                                                style="font-size:12.0pt"><span new="" roman="" times="">:<br/>
                        <b>Pravidl&aacute; a podmienky na utv&aacute;ranie &scaron;tudijn&yacute;ch pl&aacute;nov</b> (str. 16), <b>Harmonogram bakal&aacute;rskeho &scaron;t&uacute;dia</b> (str. 25).</span></span></span></span></span></p>

                        <p style="margin: 0cm 0cm 10pt;"><span style="font-family:Times New Roman,Times,serif;"><span
                                        style="font-size:11pt"><span style="line-height:12.0pt"><u><span
                                                    style="font-size:12.0pt"><span new="" roman="" times="">D&ocirc;ležit&eacute; na preč&iacute;tanie v &scaron;tudijnom programe pre &scaron;tudentov inžinierskeho &scaron;t&uacute;dia:</span></span></u><br/>
                        <span style="font-size:12.0pt"><span new="" roman="" times=""><b>Pravidl&aacute; a podmienky na utv&aacute;ranie &scaron;tudijn&yacute;ch pl&aacute;nov</b> (str. 151), <b>Harmonogram inžinierskeho &scaron;t&uacute;dia</b> (str. 156).</span></span></span></span></span></p>
                    </textarea>
                </div>
            </form>

        </div>
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-md">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Formulár</h4>
                    </div>
                    <div class="modal-body">
                        <div id="form_container"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="saveToDbFormButton" class="btn btn-default" data-toggle="modal"
                                data-target="#saveFormModal">Uložiť formulár a šablónu do databázy
                        </button>
                        <button type="button" id="acceptFormButton" class="btn btn-default">Potvrdiť</button>
                        <button type="button" id="closeFormButton" class="btn btn-default" data-dismiss="modal">
                            Zatvoriť
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div id="saveFormModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-sm">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Uložiť Formulár</h4>
                    </div>
                    <div class="modal-body">
                        <div id="form_container_save">
                            <div class="form-group form-inline saveFileDiv">
                                <label for="name">Názov formuláru:<input type="text" class="form-control" id="fileName"></label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="saveFileButton" class="btn btn-default">Uložiť</button>
                        <button type="button" id="cancelFileButton" class="btn btn-default" data-dismiss="modal">
                            Zrušiť
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="assets/global/dependencies/jquery/jquery-3.2.1.min.js"></script>
    <!--<script src="assets/global/dependencies/materialize-src/js/bin/materialize.min.js"></script>-->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    //only for datepicker
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="assets/global/dependencies/ckeditor/ckeditor.js"></script>
    <script src="assets/global/dependencies/ckeditor/plugins/find/dialogs/functionsLukas.js"></script>
    <script src="assets/global/js/plugins/FileSaver.js"></script>
    <script src="assets/global/js/plugins/jquery.wordexport.js"></script>
    <script src="dist/editorBundle.js"></script>
</div>
</body>
</html>