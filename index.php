<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

if (!isset($_SESSION["userId"])) {
    header('Location: http://rsc.sidlo.sro.sk/rscProject_PH/login.php');
    exit();
}
?>

<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="assets/index/style/index.css">
    <link rel="stylesheet" href="assets/editor/css/modalForm.css">
    <title>Main Page</title>
</head>
<body>
<!-- Modal -->
<div id="modal_templates" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Template</h3>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <h4 class="modal-title">RSC Variables</h4>
                <div class="modalDiv_rscVariables"></div>
                <div class="modalDiv_htmlPreview"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<div id="modal_forms" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Form</h3>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="modalDiv_rscVariables"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_superform" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Superform</h3>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="modalDiv_rscVariables"></div>
                <div id="form_container" class="modalDiv_form"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="button_downloadPdf" type="button" class="btn btn-default" data-dismiss="modal">Download
                    PDFs
                </button>
            </div>
        </div>
    </div>
</div>
<div class="wrapper">
    <div>
        <ul class="navigationBar">
            <li class="navigationBar_items"><a href="http://rsc.sidlo.sro.sk/rscProject_PH/index.php">Index</a></li>
            <li class="navigationBar_items"><a href="http://rsc.sidlo.sro.sk/rscProject_PH/editor.php">Editor</a></li>
            <li class="navigationBar_items"><a href=""></a></li>
            <li id="logOut" class="navigationBar_items loggedUser">
                <a>
                    <span class="glyphicon glyphicon-log-out icons"></span>
                    <?php if (isset($_SESSION["userId"])) echo ($_SESSION["userId"] == "1") ? "Log In" : "Log Out" ?>
                </a>
            </li>
            <li class="navigationBar_items rightFloat">
                <a>
                    <span class="glyphicon glyphicon-user icons"></span>
                    <?php if (isset($_SESSION["username"])) echo $_SESSION["username"]; ?>
                </a>
            </li>
        </ul>
    </div>

    <div class="tables">
        <div class="div_buttons">
            <button id="button_superform" type="button" class="btn btn-default">Generate Superform</button>
        </div>
        <div id="div_templates">
            <table id="table_templates">
                <caption><h1>Templates</h1></caption>
                <thead>
                <tr>
                    <th class="table_forms_narrow">#</th>
                    <th class="table_forms_narrow"><label for="checkbox_formsTemplates"></label><input type="checkbox"
                                                                                                       id="checkbox_formsTemplates">
                    </th>
                    <th>Name</th>
                    <th class="table_forms_narrow">RSC Num</th>
                    <th class="table_forms_narrow"></th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
        <div id="div_forms">
            <table id="table_forms">
                <caption><h1>Forms</h1></caption>
                <thead>
                <tr>
                    <th class="table_forms_narrow">#</th>
                    <th class="table_forms_narrow"><label for="checkbox_formsRows"></label><input type="checkbox"
                                                                                                  id="checkbox_formsRows">
                    </th>
                    <th>Name</th>
                    <th class="table_forms_narrow">RSC Num</th>
                    <th class="table_forms_narrow"></th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
<script src="assets/global/dependencies/jquery/jquery-3.2.1.min.js"></script>
<!--<script src="assets/global/dependencies/materialize-src/js/bin/materialize.min.js"></script>-->
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="dist/indexBundle.js"></script>
</body>
</html>